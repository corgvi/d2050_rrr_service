package com.tec4u.d2050.rrr.service.rrr;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.tec4u.d2050.rrr.ConstantString;
import com.tec4u.d2050.rrr.model.MassBalance;
import com.tec4u.d2050.rrr.model.TreeNode;
import com.tec4u.d2050.rrr.model.project.VdaBalance;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class VdaBalanceService {
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private RrrTreeService treeService;

	// @Autowired
	// private JsonNodeModificationHelper jsonNodeModificationHelper;

	@Value("${d2050.service.baseurl}")
	private String d2050ServiceBaseurl;

	public JsonNode calculateBalance(int variantId) {
		log.info("Recalculating balance for variant {}...", variantId);
		ObjectMapper mapper = new ObjectMapper();

		TreeNode tree = treeService.getTreeNodeByVariant(variantId);
		MassBalance balance = processNode(tree);
		ObjectNode resultNode = mapper.valueToTree(balance);
		log.info("Done.");
		return resultNode;
	}

	private MassBalance processNode(TreeNode node) {
		log.info("Processing interior node {}...", node.getId());
		MassBalance result = new MassBalance();
		if (node.getChildren().isEmpty()) {
			result.addAll(processLeaf(node));
		} else {
			for (TreeNode child : node.getChildren()) {
				result.addAll(processNode(child));
			}
		}
		if (node.getId() != null && node.getId() !=0) {
			writeVdaBalance(node.getId(), result);
		}
		return result;
	}

	private MassBalance processLeaf(TreeNode node) {
		log.info("Processing leaf {}...", node.getId());
		final String uriComponentMain = d2050ServiceBaseurl + "project/componentMain/" + node.getId();
		MassBalance result = new MassBalance();
		try {
			JsonNode componentNode = restTemplate.getForObject(new URI(uriComponentMain), JsonNode.class);
			if (componentNode != null) {
				if (componentNode.get(ConstantString.MATERIAL_MAIN_ID) != null) {
					Integer materialGroupId = componentNode.get(ConstantString.MATERIAL_MAIN_ENTITY)
							.get(ConstantString.MATERIAL_GROUP_ID).asInt(-1);
					Double mass = componentNode.get(ConstantString.MASSE).asDouble(0.0);

					log.info("Mass: {}, Material: {}", mass, materialGroupId);
					result.add(materialGroupId, mass);
					writeVdaBalance(node.getId(), result);
					return result;
				}
			}
		} catch (URISyntaxException e) {
			log.error(e.getMessage(), e);
		}

		return result;
	}

	private void writeVdaBalance(Integer componentId, MassBalance mb) {
		final String uriVdaBalance = d2050ServiceBaseurl + "project/vdaBalance";
		log.info("URI: {}", uriVdaBalance);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ObjectMapper mapper = new ObjectMapper();
		for (Map.Entry<Long, Double> entry : mb.getGroups().entrySet()) {
			Integer materialGroupId = entry.getKey().intValue();
			Double mass = entry.getValue();
			VdaBalance balance = new VdaBalance();
			balance.setComponentId(componentId);
			balance.setMaterialGroupId(materialGroupId);
			balance.getId().setComponentId(componentId);
			balance.getId().setMaterialGroupId(materialGroupId);
			balance.setValue(mass);
			JsonNode bnode = mapper.valueToTree(balance);
			log.info("bnode: {}", bnode.toString());
			try {
				// HttpEntity<String> request = new HttpEntity<String>(bnode.toString(),
				// headers);
				JsonNode vbNode = restTemplate.postForObject(new URI(uriVdaBalance), bnode, JsonNode.class);
				log.info("Response: {}", vbNode.toString());
			} catch (URISyntaxException e) {
				log.error(e.getMessage(), e);
			}
		}
	}

}
