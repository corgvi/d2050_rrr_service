package com.tec4u.d2050.rrr.am;

public class MaterialQuantity {
	private double value;
	private MaterialUnit unit;

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public MaterialUnit getUnit() {
		return unit;
	}

	public void setUnit(MaterialUnit unit) {
		this.unit = unit;
	}

}
