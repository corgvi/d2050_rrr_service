package com.tec4u.d2050.rrr.am;

public class MaterialUnit {
	private String datatypeURI;
	private String lexicalValue;

	public String getDatatypeURI() {
		return datatypeURI;
	}

	public void setDatatypeURI(String datatypeURI) {
		this.datatypeURI = datatypeURI;
	}

	public String getLexicalValue() {
		return lexicalValue;
	}

	public void setLexicalValue(String lexicalValue) {
		this.lexicalValue = lexicalValue;
	}

}
