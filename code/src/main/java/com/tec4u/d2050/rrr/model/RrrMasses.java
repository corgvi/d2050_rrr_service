package com.tec4u.d2050.rrr.model;

import consulting.sit.catenax.model.ModelBaseInterface;

public class RrrMasses implements ModelBaseInterface<Integer> {

	private Integer id;
	private double mm, mtr, mte, mr, mp, md;

	public double getMm() {
		return mm;
	}

	public void setMm(double mm) {
		this.mm = mm;
	}

	public double getMtr() {
		return mtr;
	}

	public void setMtr(double mtr) {
		this.mtr = mtr;
	}

	public double getMte() {
		return mte;
	}

	public void setMte(double mte) {
		this.mte = mte;
	}

	public double getMr() {
		return mr;
	}

	public void setMr(double mr) {
		this.mr = mr;
	}

	public double getMp() {
		return mp;
	}

	public void setMp(double mp) {
		this.mp = mp;
	}

	public double getMd() {
		return md;
	}

	public void setMd(double md) {
		this.md = md;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
