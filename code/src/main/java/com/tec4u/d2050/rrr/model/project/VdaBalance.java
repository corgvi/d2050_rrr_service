package com.tec4u.d2050.rrr.model.project;

public class VdaBalance {

	private VdaBalancePk id;
	private Integer componentId;
	private Integer materialGroupId;
	private Double value;

	public VdaBalance() {
		id = new VdaBalancePk();
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public Integer getMaterialGroupId() {
		return materialGroupId;
	}

	public void setMaterialGroupId(Integer materialGroupId) {
		this.materialGroupId = materialGroupId;
	}

	public VdaBalancePk getId() {
		return id;
	}

	public void setId(VdaBalancePk id) {
		this.id = id;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

}
