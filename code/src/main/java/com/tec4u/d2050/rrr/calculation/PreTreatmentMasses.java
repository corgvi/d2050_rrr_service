package com.tec4u.d2050.rrr.calculation;

public class PreTreatmentMasses {

	private double fluid, battery, oilFilter, lpg, cng, tyre, catalytic;

	public double mP() {
		return fluid + battery + oilFilter + lpg + cng + tyre + catalytic;
	}

	public double getFluid() {
		return fluid;
	}

	public void setFluid(double fluid) {
		this.fluid = fluid;
	}

	public double getBattery() {
		return battery;
	}

	public void setBattery(double battery) {
		this.battery = battery;
	}

	public double getOilFilter() {
		return oilFilter;
	}

	public void setOilFilter(double oilFilter) {
		this.oilFilter = oilFilter;
	}

	public double getLpg() {
		return lpg;
	}

	public void setLpg(double lpg) {
		this.lpg = lpg;
	}

	public double getCng() {
		return cng;
	}

	public void setCng(double cng) {
		this.cng = cng;
	}

	public double getTyre() {
		return tyre;
	}

	public void setTyre(double tyre) {
		this.tyre = tyre;
	}

	public double getCatalytic() {
		return catalytic;
	}

	public void setCatalytic(double catalytic) {
		this.catalytic = catalytic;
	}

}
