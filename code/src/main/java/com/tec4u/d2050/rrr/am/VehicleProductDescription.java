package com.tec4u.d2050.rrr.am;

import java.time.LocalDate;

// urn:bamm:io.catenax.vehicle.product_description:1.0.1#ProductDescription

public class VehicleProductDescription {
	 private String catenaXId, bodyVariant, fuel, vehicleModel, anonymizedIdentifier;
	 private LocalDate productionDateGMT;
		private ProductDescriptionEngine engine;

		public String getCatenaXId() {
			return catenaXId;
		}

		public void setCatenaXId(String catenaXId) {
			this.catenaXId = catenaXId;
		}

		public String getBodyVariant() {
			return bodyVariant;
		}

		public void setBodyVariant(String bodyVariant) {
			this.bodyVariant = bodyVariant;
		}

		public String getFuel() {
			return fuel;
		}

		public void setFuel(String fuel) {
			this.fuel = fuel;
		}

		public String getVehicleModel() {
			return vehicleModel;
		}

		public void setVehicleModel(String vehicleModel) {
			this.vehicleModel = vehicleModel;
		}

		public String getAnonymizedIdentifier() {
			return anonymizedIdentifier;
		}

		public void setAnonymizedIdentifier(String anonymizedIdentifier) {
			this.anonymizedIdentifier = anonymizedIdentifier;
		}

		public LocalDate getProductionDateGMT() {
			return productionDateGMT;
		}

		public void setProductionDateGMT(LocalDate productionDateGMT) {
			this.productionDateGMT = productionDateGMT;
		}

		public ProductDescriptionEngine getEngine() {
			return engine;
		}

		public void setEngine(ProductDescriptionEngine engine) {
			this.engine = engine;
		} 
}
