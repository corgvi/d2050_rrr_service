package com.tec4u.d2050.rrr.service.rrr;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tec4u.d2050.rrr.ConstantString;
import com.tec4u.d2050.rrr.calculation.IsoChartData;
import com.tec4u.d2050.rrr.calculation.IsoGroupMasses;
import com.tec4u.d2050.rrr.model.TreeNode;

import consulting.sit.catenax.helper.IsoGroups;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IsoMassesService {
	@Autowired
	private RestTemplate restTemplate;

	// @Autowired
	// private JsonNodeModificationHelper jsonNodeModificationHelper;

	@Value("${d2050.service.baseurl}")
	private String d2050ServiceBaseurl;

	public JsonNode isoMassesForVariant(int variantId) {
		log.info("Calculating ISO masses for variant {}...", variantId);
		ObjectMapper mapper = new ObjectMapper();
		IsoGroupMasses isoMasses = computeIsoMassesForVariant(variantId);
		log.info("Done.");
		return mapper.valueToTree(isoMasses);
	}

	public JsonNode isoChartForVariant(int variantId) {
		log.info("Calculating ISO masses for variant {}...", variantId);
		ObjectMapper mapper = new ObjectMapper();
		IsoGroupMasses isoMasses = computeIsoMassesForVariant(variantId);
		log.info("Done.");
		return mapper.valueToTree(new IsoChartData(isoMasses));
	}

	public JsonNode isoMassesForComponent(int componentId) {
		log.info("Calculating ISO masses for component {}...", componentId);
		ObjectMapper mapper = new ObjectMapper();
		IsoGroupMasses isoMasses = computeIsoMasses(componentId);
		return mapper.valueToTree(isoMasses);
	}

	public JsonNode isoChartForComponent(int componentId) {
		log.info("Calculating ISO chart for component {}...", componentId);
		ObjectMapper mapper = new ObjectMapper();
		IsoGroupMasses isoMasses = computeIsoMasses(componentId);
		return mapper.valueToTree(new IsoChartData(isoMasses));
	}

	private IsoGroupMasses computeIsoMassesForVariant(int variantId) {
		IsoGroupMasses isoMasses = new IsoGroupMasses();
		try {
			JsonNode children = restTemplate.getForObject(new URI(childComponents(variantId)), JsonNode.class);
			log.info("children: {}", children.toString());
			if (children != null) {
				children.forEach(cd -> {
					log.info("cd: {}", cd.toString());
					if (cd != null && cd.get(ConstantString.ID) != null) {
						Integer dependencyId = cd.get(ConstantString.ID).asInt();
						log.info("Add: {}", dependencyId);
						isoMasses.add(computeIsoMasses(dependencyId));
					}
				});
			}
		} catch (RestClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isoMasses;
	}

	private IsoGroupMasses computeIsoMasses(int componentId) {
		IsoGroupMasses isoMasses = new IsoGroupMasses();
		try {
			JsonNode vbNode = restTemplate.getForObject(new URI(vdaBalanceForComponent(componentId)), JsonNode.class);
			log.info("vbNode: {}", vbNode.toString());
			if (vbNode != null) {
				vbNode.forEach(vb -> {
					try {
						URI materialGroupUri = new URI(materialGroup(vb.get(ConstantString.MATERIAL_GROUP_ID).asInt()));
						log.info("URI: {}", materialGroupUri.toString());
						JsonNode mgNode = restTemplate.getForObject(materialGroupUri, JsonNode.class);
						if (mgNode != null) {
							log.info("mg: {}", mgNode.toString());
							String vdaUg = mgNode.get(ConstantString.VDA_UG).asText("");
							Double mass = vb.get("value").asDouble(0.0);
							log.info("vdaUg: {}, iso: {}, mass: {}", vdaUg, IsoGroups.fromVda(vdaUg), mass);
							isoMasses.add(IsoGroups.fromVda(vdaUg), mass);
						}
					} catch (RestClientException e) {
						log.error(e.getMessage(), e);
						e.printStackTrace();
					} catch (URISyntaxException e) {
						log.error(e.getMessage(), e);
						e.printStackTrace();
					}
					log.info("vb: {}", vb.toString());
				});
			}
		} catch (URISyntaxException e) {
			log.error(e.getMessage(), e);
		}
		return isoMasses;
	}

	private String childComponents(int variantId) {
		return d2050ServiceBaseurl + "project/componentMain/variant/" + variantId+"/first";
	}

	private String vdaBalanceForComponent(int componentId) {
		return d2050ServiceBaseurl + "project/vdaBalance/component/" + componentId;
	}

	private String materialGroup(int materialGroupId) {
		return d2050ServiceBaseurl + "glossary/materialGroup/" + materialGroupId;
	}

	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public String getD2050ServiceBaseurl() {
		return d2050ServiceBaseurl;
	}

	public void setD2050ServiceBaseurl(String d2050ServiceBaseurl) {
		this.d2050ServiceBaseurl = d2050ServiceBaseurl;
	}

}
