package com.tec4u.d2050.rrr.model;

import java.util.HashMap;
import java.util.Map;

public class MassBalance {
	
	private Map<Long, Double> groups;

	public MassBalance() {
		groups=new HashMap<>();
	}
	
	public void add(long group, double value) {
		groups.put(group, groups.getOrDefault(group, 0.0)+value);
	}
	
	public void addAll(MassBalance other) {
		for(Map.Entry<Long, Double> entry:other.groups.entrySet()) {
			this.add(entry.getKey(), entry.getValue());
		}
	}
	
	public Map<Long, Double> getGroups() {
		return groups;
	}

	public void setGroups(Map<Long, Double> groups) {
		this.groups = groups;
	}
	
	
}
