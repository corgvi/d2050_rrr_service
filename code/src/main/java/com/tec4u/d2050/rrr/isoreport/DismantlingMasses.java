package com.tec4u.d2050.rrr.isoreport;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DismantlingMasses {
	private List<DismantlingEntry> components;
	
	public DismantlingMasses() {
		components = new ArrayList<>();
	}

	public List<DismantlingEntry> getComponents() {
		return components;
	}

	public void setComponents(List<DismantlingEntry> components) {
		this.components = components;
	}

	@JsonProperty("mD1")
	public double mD1() {
		double sum = 0.0;
		for (int i = 0; i < 5 && i < components.size(); i++) {
			sum += components.get(i).getMass();
		}
		return sum;
	}

	@JsonProperty("mD2")
	public double mD2() {
		double sum = 0.0;
		for (int i = 5; i < 10 && i < components.size(); i++) {
			sum += components.get(i).getMass();
		}
		return sum;
	}

	@JsonProperty("mDx")
	public double mDx() {
		double sum = 0.0;
		for (int i = 10; i < components.size(); i++) {
			sum += components.get(i).getMass();
		}
		return sum;
	}

	@JsonProperty("mD")
	public double mD() {
		return mD1() + mD2() + mDx();
	}
}
