package com.tec4u.d2050.rrr.am;

import java.time.Instant;

public class ProductDescriptionMileage {
	private String mileagePhase;
	private Instant mileageTimestamp;
	private double mileageDistance;

	public String getMileagePhase() {
		return mileagePhase;
	}

	public void setMileagePhase(String mileagePhase) {
		this.mileagePhase = mileagePhase;
	}

	public Instant getMileageTimestamp() {
		return mileageTimestamp;
	}

	public void setMileageTimestamp(Instant mileageTimestamp) {
		this.mileageTimestamp = mileageTimestamp;
	}

	public double getMileageDistance() {
		return mileageDistance;
	}

	public void setMileageDistance(double mileageDistance) {
		this.mileageDistance = mileageDistance;
	}
}
