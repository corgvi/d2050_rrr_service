package com.tec4u.d2050.rrr.model;

import java.util.ArrayList;
import java.util.List;

public class TreeNode {

	private Integer id;
	private List<TreeNode> children;

	public TreeNode() {
		children = new ArrayList<>();
	}

	public List<TreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
