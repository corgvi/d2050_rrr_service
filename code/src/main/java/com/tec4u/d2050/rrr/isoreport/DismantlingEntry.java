package com.tec4u.d2050.rrr.isoreport;

public class DismantlingEntry {
	private String name;
	private double mass;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getMass() {
		return mass;
	}

	public void setMass(double mass) {
		this.mass = mass;
	}
}
