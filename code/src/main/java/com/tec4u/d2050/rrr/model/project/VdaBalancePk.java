package com.tec4u.d2050.rrr.model.project;

public class VdaBalancePk {
	
	private Integer componentId;
	private Integer materialGroupId;
	
	
	public Integer getComponentId() {
		return componentId;
	}
	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}
	public Integer getMaterialGroupId() {
		return materialGroupId;
	}
	public void setMaterialGroupId(Integer materialGroupId) {
		this.materialGroupId = materialGroupId;
	}
	
}
