package com.tec4u.d2050.rrr.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value= {"componentMainEntity"})
public class ComponentDepend {
	private Integer id;
	private Long componentId, abhaengigkeit;
	private boolean konstruktiv, strategisch;
	private Long zbPart;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getComponentId() {
		return componentId;
	}

	public void setComponentId(Long componentId) {
		this.componentId = componentId;
	}

	public Long getAbhaengigkeit() {
		return abhaengigkeit;
	}

	public void setAbhaengigkeit(Long abhaengigkeit) {
		this.abhaengigkeit = abhaengigkeit;
	}

	public boolean isKonstruktiv() {
		return konstruktiv;
	}

	public void setKonstruktiv(boolean konstruktiv) {
		this.konstruktiv = konstruktiv;
	}

	public boolean isStrategisch() {
		return strategisch;
	}

	public void setStrategisch(boolean strategisch) {
		this.strategisch = strategisch;
	}

	public Long getZbPart() {
		return zbPart;
	}

	public void setZbPart(Long zbPart) {
		this.zbPart = zbPart;
	}

}
