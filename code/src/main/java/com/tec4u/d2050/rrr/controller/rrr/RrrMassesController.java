package com.tec4u.d2050.rrr.controller.rrr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.tec4u.d2050.rrr.service.rrr.RrrMassesService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/rrr/rrrmasses")
@Slf4j
public class RrrMassesController {
	@Autowired
	private RrrMassesService rrrMassesService;

	@GetMapping("/{id}")
	private JsonNode getVariants(@PathVariable int id) {
		return rrrMassesService.getRrrMasses(id);
	}

}
