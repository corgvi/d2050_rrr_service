package com.tec4u.d2050.rrr.controller.rrr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.tec4u.d2050.rrr.service.rrr.VdaBalanceService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/rrr/vdabalance")
@Slf4j
public class VdaBalanceController {
	
	@Autowired
	private VdaBalanceService vdaBalanceService;

	@GetMapping("/variant/{id}")
	private JsonNode getTreeByVariant(@PathVariable int id) {
		return vdaBalanceService.calculateBalance(id);
	}

}
