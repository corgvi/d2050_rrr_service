package com.tec4u.d2050.rrr.service.rrr;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeCreator;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.tec4u.d2050.rrr.calculation.Masses;
import com.tec4u.d2050.rrr.controller.rrr.RrrMassesController;
import com.tec4u.d2050.rrr.model.ComponentDepend;

import com.tec4u.d2050.rrr.ConstantString;
import consulting.sit.catenax.helper.JsonNodeModificationHelper;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RrrMassesService {
	@Autowired
	private RestTemplate restTemplate;

	//@Autowired
	//private JsonNodeModificationHelper jsonNodeModificationHelper;

	@Value("${d2050.service.baseurl}")
	private String d2050ServiceBaseurl;

	public JsonNode getRrrMasses(int id) {
		ObjectMapper mapper = new ObjectMapper();
		final String uri = d2050ServiceBaseurl + "project/componentDepend";
		List<ComponentDepend> cds = new ArrayList<>();
		try {
			JsonNode node = restTemplate.getForObject(new URI(uri), JsonNode.class);
			Iterator<JsonNode> el = node.elements();
			while(el.hasNext()) {
				JsonNode n = el.next();
				log.info(n.toString());
				cds.add(mapper.treeToValue(n, ComponentDepend.class));
			}
		} catch (URISyntaxException e) {
			e.printStackTrace();
			log.warn("Could not fetch component list.");
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Masses masses = new Masses();
		JsonNode node = mapper.valueToTree(masses);
		return node;
		/*
		final String uri = d2050ServiceBaseurl + "project/project";
		try {
			JsonNode node = restTemplate.getForObject(new URI(uri), JsonNode.class);
			node.forEach(e -> {
				if (e instanceof ObjectNode) {
					ObjectNode objectNode = (ObjectNode) e;

					
					//jsonNodeModificationHelper.removeAllBut(objectNode, List.of("id", "name"));
				}
			});
			return node;
		} catch (URISyntaxException e) {
			e.printStackTrace();
			return null;
		}
		//return null;*/
	}
	
	public JsonNode getMasses() {
		return null;
	}
}
