package com.tec4u.d2050.rrr.calculation;

import consulting.sit.catenax.helper.IsoGroups;

public class IsoGroupMasses {

	private double metal;
	private double polymer;
	private double elastomer;
	private double glass;
	private double fluid;
	private double mon;
	private double other;

	public double mm() {
		return metal;
	}

	public IsoGroupMasses add(IsoGroupMasses other) {
		this.metal += other.metal;
		this.polymer += other.polymer;
		this.elastomer += other.elastomer;
		this.glass += other.glass;
		this.fluid += other.fluid;
		this.mon += other.mon;
		this.other += other.other;
		return this;
	}

	public IsoGroupMasses add(int isoGroup, double mass) {
		switch (isoGroup) {
		case IsoGroups.METALS:
			metal+=mass;
			break;
		case IsoGroups.POLYMERS:
			polymer+=mass;
			break;
		case IsoGroups.ELASTOMERS:
			elastomer+=mass;
			break;
		case IsoGroups.GLASS:
			glass+=mass;
			break;
		case IsoGroups.FLUIDS:
			fluid+=mass;
			break;
		case IsoGroups.MON:
			mon+=mass;
			break;
		case IsoGroups.OTHERS:
			other+=mass;
			break;
		default:
		}
		return this;
	}

	public double mtr(double provenTech) {
		return nonMetal() * provenTech;
	}

	public double mte(double provenTech) {
		return reducedByProvenTech(polymer + elastomer + fluid + mon, provenTech);
	}

	public double mr(double provenTech) {
		return reducedByProvenTech(glass + other, provenTech);
	}

	private double nonMetal() {
		return polymer + elastomer + glass + fluid + mon + other;
	}

	private double reducedByProvenTech(double value, double provenTech) {
		return value - value * provenTech;
	}

	// getters and setters

	public double getMetal() {
		return metal;
	}

	public void setMetal(double metal) {
		this.metal = metal;
	}

	public double getElastomer() {
		return elastomer;
	}

	public void setElastomer(double elastomer) {
		this.elastomer = elastomer;
	}

	public double getGlass() {
		return glass;
	}

	public void setGlass(double glass) {
		this.glass = glass;
	}

	public double getFluid() {
		return fluid;
	}

	public void setFluid(double fluid) {
		this.fluid = fluid;
	}

	public double getMon() {
		return mon;
	}

	public void setMon(double mon) {
		this.mon = mon;
	}

	public double getOther() {
		return other;
	}

	public void setOther(double other) {
		this.other = other;
	}

	public double getPolymer() {
		return polymer;
	}

	public void setPolymer(double polymer) {
		this.polymer = polymer;
	}

}
