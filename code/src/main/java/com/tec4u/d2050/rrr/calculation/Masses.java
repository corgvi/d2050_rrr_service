package com.tec4u.d2050.rrr.calculation;

public class Masses {
	private PreTreatmentMasses pretreatment;
	private RrrMasses rrr;
	private IsoGroupMasses isogroup;

	public Masses() {
		pretreatment = new PreTreatmentMasses();
		rrr = new RrrMasses();
		isogroup = new IsoGroupMasses();
	}

	public PreTreatmentMasses getPretreatment() {
		return pretreatment;
	}

	public void setPretreatment(PreTreatmentMasses pretreatment) {
		this.pretreatment = pretreatment;
	}

	public RrrMasses getRrr() {
		return rrr;
	}

	public void setRrr(RrrMasses rrr) {
		this.rrr = rrr;
	}

	public IsoGroupMasses getIsogroup() {
		return isogroup;
	}

	public void setIsogroup(IsoGroupMasses isogroup) {
		this.isogroup = isogroup;
	}
}
