package com.tec4u.d2050.rrr;


public class ConstantString {
    private ConstantString() {
    }

    public static final String COMPONENT_MAIN_BENENNUNG_ENTITY = "componentMainBenennungEntity";
    public static final String VARIANT_DETAIL_ENTITY = "variantDetailEntity";

    public static final String VARIANT_AIRBAG = "variantAirbags";
    public static final String MATERIAL_MAIN_ENTITY = "materialMainEntity";
    public static final String MATERIAL_GROUP_ENTITY = "materialGroupEntity";

    public static final String MATERIAL_MAIN_LANGUAGE_MAPS = "materialMainLanguageMaps";
    public static final String PART_LANGUAGE_MAPS = "partLanguageMaps";

    public static final String ID = "id";
    public static final String PROJECT_ID = "projectId";
    public static final String VARIANT_ID = "variantId";
    public static final String CREATED_BY = "createdBy";
    public static final String CREATED_AT = "createdAt";
    public static final String MODIFIED_BY = "modifiedBy";
    public static final String MODIFIED_AT = "modifiedAt";
    public static final String DESCRIPTION = "description";
    public static final String NAME = "name";
    public static final String COMPONENT_ID = "componentId";
    public static final String BENENNUNG_ORIGINAL = "benennungOrginal";
    public static final String LEVEL_IN_BOM = "levelInBom";
    public static final String TOP = "top";
    public static final String WEIGHT = "weight";
    public static final String MASSE = "masse";
    public static final String ABHAENGIGKEIT = "abhaengigkeit";
    public static final String VALUE = "value";
    public static final String CHILDREN = "children";
    public static final String LANGUAGE = "language";
    public static final String FAHRZEUG_BEZEICHNUNG = "fahrzeugbezeichnung";
    public static final String DISMANTLINGSTUDY = "dismantlingStudy";
    public static final String UNTERSUCHUNGSTART = "untersuchungStart";
    public static final String UNTERSUCHUNGENDE = "untersuchungEnde";
    public static final String BATTERIETYP12V = "batterieTyp12v";
    public static final String BATTERIETYPHV = "batterieTypHv";
    public static final String BATTERIETYP24V = "batterieTyp24v";
    public static final String BATTERIETYP48V = "batterieTyp48v";
    public static final String VERSION = "version";
    public static final String CXPROJECT = "cxProject";
    public static final String MATERIAL_MAIN_ID = "materialMainId";
    public static final String MATERIAL_GROUP_ID = "materialGroupId";
    public static final String VDA_UG = "vdaUg";
}
