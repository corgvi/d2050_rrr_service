package com.tec4u.d2050.rrr.calculation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RrrCalculations {

	private static final double PROVEN_TECH = 0.0;

	private List<Long> nodes;
	private List<Long> dismantling;
	private Set<Long> selected;
	private IsoGroupMasses totalIso;
	private RrrMasses totalRrr;
	private PreTreatmentMasses totalPretreatment;

	/**
	 * Creates an empty instance
	 */
	public RrrCalculations() {
		nodes = new ArrayList<>();
		dismantling = new ArrayList<>();
		selected = new HashSet<>();
		totalIso = new IsoGroupMasses();
		totalRrr = new RrrMasses();
	}

	/**
	 * computes values for the list of nodes, taking the current selection into
	 * account
	 */
	public void compute() {
		for (Long id : nodes) {
			processComponent(id);
		}
	}

	/**
	 * Processes one component
	 * 
	 * @param id node id to pocess
	 */
	public void processComponent(long id) {
		// processing common to all
		IsoGroupMasses ciso = new IsoGroupMasses();

		// TODO read values for node
		ciso.setElastomer(0.0);
		ciso.setFluid(0.0);
		ciso.setGlass(0.0);
		ciso.setMetal(0.0);
		ciso.setMon(0.0);
		ciso.setOther(0.0);
		ciso.setPolymer(0.0);

		totalIso.add(ciso);

		if (!selected(id)) {
			// only those not selected
			processNotSelected(id, ciso);
		}
	}

	/**
	 * Handles calculations specific to nodes that have not been selected for
	 * pretreatment or dismantling
	 * 
	 * @param id node id to pocess
	 */
	public void processNotSelected(long id, IsoGroupMasses ciso) {
		RrrMasses crrr = new RrrMasses();
		double provenTech = provenTech(id);
		crrr.setMm(ciso.mm());
		crrr.setMr(ciso.mr(provenTech));
		crrr.setMte(ciso.mte(provenTech));
		crrr.setMtr(ciso.mtr(provenTech));
	}

	/**
	 * Checks if a node has been selected for pretreatment or dismantling
	 * 
	 * @param id node id to check
	 * @return true if the node is selected, false otherwise
	 */
	private boolean selected(long id) {
		return selected.contains(id);
	}

	/**
	 * 
	 * @param id node id
	 * @return proven technology factor (0.0-1.0)
	 */
	private double provenTech(long id) {
		return PROVEN_TECH;
	}

	public List<Long> getNodes() {
		return nodes;
	}

	public void setNodes(List<Long> nodes) {
		this.nodes = nodes;
	}

	public Set<Long> getSelected() {
		return selected;
	}

	public void setSelected(Set<Long> selected) {
		this.selected = selected;
	}

	public IsoGroupMasses getTotalIso() {
		return totalIso;
	}

	public void setTotalIso(IsoGroupMasses totalIso) {
		this.totalIso = totalIso;
	}

	public RrrMasses getTotalRrr() {
		return totalRrr;
	}

	public void setTotalRrr(RrrMasses rrrMasses) {
		this.totalRrr = rrrMasses;
	}

	public List<Long> getDismantling() {
		return dismantling;
	}

	public void setDismantling(List<Long> dismantling) {
		this.dismantling = dismantling;
	}

	public PreTreatmentMasses getTotalPretreatment() {
		return totalPretreatment;
	}

	public void setTotalPretreatment(PreTreatmentMasses totalPretreatment) {
		this.totalPretreatment = totalPretreatment;
	}
}
