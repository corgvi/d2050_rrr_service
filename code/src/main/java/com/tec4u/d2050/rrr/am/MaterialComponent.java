package com.tec4u.d2050.rrr.am;

public class MaterialComponent {

	private String materialName;
	private double recycledConent;
	private String materialClass;
	private String aggregateState;
	private String materialAbbreviation;

	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}

	public double getRecycledConent() {
		return recycledConent;
	}

	public void setRecycledConent(double recycledConent) {
		this.recycledConent = recycledConent;
	}

	public String getMaterialClass() {
		return materialClass;
	}

	public void setMaterialClass(String materialClass) {
		this.materialClass = materialClass;
	}

	public String getAggregateState() {
		return aggregateState;
	}

	public void setAggregateState(String aggregateState) {
		this.aggregateState = aggregateState;
	}

	public String getMaterialAbbreviation() {
		return materialAbbreviation;
	}

	public void setMaterialAbbreviation(String materialAbbreviation) {
		this.materialAbbreviation = materialAbbreviation;
	}

}
