package com.tec4u.d2050.rrr.calculation;

import java.util.ArrayList;
import java.util.List;

public class IsoChartData {

	private List<Double> data;

	public IsoChartData() {
		data = new ArrayList<>(7);
	}

	public IsoChartData(IsoGroupMasses masses) {
		data = new ArrayList<>(7);
		data.add(masses.getMetal());
		data.add(masses.getPolymer());
		data.add(masses.getElastomer());
		data.add(masses.getGlass());
		data.add(masses.getFluid());
		data.add(masses.getMon());
		data.add(masses.getOther());
	}

	public List<Double> getData() {
		return data;
	}

	public void setData(List<Double> data) {
		this.data = data;
	}
}
