package com.tec4u.d2050.rrr.service.rrr;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.tec4u.d2050.rrr.ConstantString;
import com.tec4u.d2050.rrr.model.TreeNode;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RrrTreeService {
	@Autowired
	private RestTemplate restTemplate;

	// @Autowired
	// private JsonNodeModificationHelper jsonNodeModificationHelper;

	@Value("${d2050.service.baseurl}")
	private String d2050ServiceBaseurl;

	public JsonNode getTreeByVariant(Integer variantId) {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode resultNode = mapper.createObjectNode();

		List<Long> rootComponentIdList = getFirstChildrenFromVariant(variantId);
		// TODO: ggf. mehrere Root-Components abfragen
		ArrayNode children = mapper.createArrayNode();
		// TODO: Get root componentId from variant
		// TODO: Remove usage of variantID as first componentId
		rootComponentIdList.forEach(componentId -> {
			if (componentId != null) {
				children.add(getChildren(componentId));
			}
		});
		resultNode.set(ConstantString.CHILDREN, children);
		return resultNode;
	}

	public TreeNode getTreeNodeByVariant(Integer variantId) {
		TreeNode resultNode = new TreeNode();
		List<Long> rootComponentIdList = getFirstChildrenFromVariant(variantId);
		log.info("Found {} top level nodes.", rootComponentIdList.size());
		List<TreeNode> children = new ArrayList<>();
		rootComponentIdList.forEach(componentId -> {
			if (componentId != null) {
				log.info("Processing component {}...", componentId);
				children.addAll(getChildTreeNodes(componentId.intValue()));
			}
		});
		resultNode.setChildren(children);
		return resultNode;
	}

	public JsonNode getTreeByComponent(Integer componentId) {
		return getChildren(componentId);
	}

	private List<Long> getFirstChildrenFromVariant(int variantId) {
		List<Long> resultList = new ArrayList<>();
		String componentUri = d2050ServiceBaseurl + "project/componentMain/variant/" + variantId + "/first";
		log.info(componentUri);
		try {
			JsonNode componentNode = restTemplate.getForObject(new URI(componentUri), JsonNode.class);
			if (componentNode != null) {
				componentNode.forEach(e -> resultList.add(e.get(ConstantString.ID).asLong()));
			}
		} catch (URISyntaxException e) {
			log.error(e.getMessage(), e);
		}
		return resultList;
	}

	private JsonNode getChildren(long componentId) {
		// Request URIs
		final String uriComponent = d2050ServiceBaseurl + "project/componentMain/" + componentId;
		final String uriComponentDepend = d2050ServiceBaseurl + "project/componentDepend/children/" + componentId;
		try {
			// request Component
			JsonNode component = restTemplate.getForObject(new URI(uriComponent), JsonNode.class);

			// Create children array
			ObjectMapper mapper = new ObjectMapper();
			ArrayNode children = mapper.createArrayNode();

			// request children
			JsonNode componentDepend = restTemplate.getForObject(new URI(uriComponentDepend), JsonNode.class);
			if (componentDepend != null) {
				componentDepend.forEach(cd -> {
					if (cd != null && cd.get(ConstantString.ABHAENGIGKEIT) != null) {
						children.add(getChildren(cd.get(ConstantString.ABHAENGIGKEIT).asLong()));
					}
				});
			}

			// Join Elements to JsonNode
			ObjectNode returnNode = mapper.createObjectNode();
			returnNode.put("id", getId(component));
			returnNode.set(ConstantString.CHILDREN, children);

			return returnNode;
		} catch (URISyntaxException e) {
			log.error(e.getMessage(), e);
			return null;
		}
	}

	private List<TreeNode> getChildTreeNodes(int componentId) {
		//final String uriComponent = d2050ServiceBaseurl + "project/componentMain/" + componentId;
		final String uriComponentDepend = d2050ServiceBaseurl + "project/componentDepend/children/" + componentId;
		List<TreeNode> result = new ArrayList<>();
		try {
			//JsonNode component = restTemplate.getForObject(new URI(uriComponent), JsonNode.class);
			JsonNode componentDepend = restTemplate.getForObject(new URI(uriComponentDepend), JsonNode.class);
			if (componentDepend != null) {
				if(componentDepend.isEmpty()) {
					log.info("Node {} is leaf node.", componentId);
					TreeNode leaf = new TreeNode();
					leaf.setId(componentId);
					result.add(leaf);
				}
				componentDepend.forEach(cd -> {
					if (cd != null && cd.get(ConstantString.ABHAENGIGKEIT) != null) {
						TreeNode node = new TreeNode();
						node.setId(Integer.parseInt(cd.get(ConstantString.COMPONENT_ID).asText()));
						Integer dependencyId = cd.get(ConstantString.ABHAENGIGKEIT).asInt();
						List<TreeNode> children = getChildTreeNodes(dependencyId);
						
						node.getChildren().addAll(children);
						result.add(node);
						log.info("{} children so far.", result.size());
					}
				});
			}
			return result;
		} catch (URISyntaxException e) {
			log.error(e.getMessage(), e);
		}
		return new ArrayList<>(0);
	}

	/**
	 * Returns id of component. Check if componentId exists and return it.
	 *
	 * @param component JsonNode to look for the id
	 * @return id from the given component, null if componentId is not found
	 */
	private Long getId(JsonNode component) {
		if (component != null && component.get(ConstantString.ID) != null) {
			return component.get(ConstantString.ID).asLong();
		}
		return null;
	}

}
