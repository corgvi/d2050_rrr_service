package com.tec4u.d2050.rrr.isoreport;

import com.tec4u.d2050.rrr.calculation.IsoGroupMasses;
import com.tec4u.d2050.rrr.calculation.PreTreatmentMasses;

public class IsoReportData {
	
	private String brandName;
	private String variant;
	private double vehicleMass;
	private IsoGroupMasses materials;
	private PreTreatmentMasses preTreatment;

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getVariant() {
		return variant;
	}

	public void setVariant(String variant) {
		this.variant = variant;
	}

	public double getVehicleMass() {
		return vehicleMass;
	}

	public void setVehicleMass(double vehicleMass) {
		this.vehicleMass = vehicleMass;
	}

	public IsoGroupMasses getMaterials() {
		return materials;
	}

	public void setMaterials(IsoGroupMasses materials) {
		this.materials = materials;
	}

	public PreTreatmentMasses getPreTreatment() {
		return preTreatment;
	}

	public void setPreTreatment(PreTreatmentMasses preTreatment) {
		this.preTreatment = preTreatment;
	}

}
