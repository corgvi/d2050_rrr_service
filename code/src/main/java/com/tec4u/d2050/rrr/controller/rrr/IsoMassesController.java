package com.tec4u.d2050.rrr.controller.rrr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.tec4u.d2050.rrr.service.rrr.IsoMassesService;
import com.tec4u.d2050.rrr.service.rrr.VdaBalanceService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/rrr/isotable/")
@Slf4j
public class IsoMassesController {
	
	@Autowired
	private IsoMassesService isoMassesService;

	@GetMapping("/variant/{id}")
	private JsonNode getIsoMassesByVariant(@PathVariable int id) {
		return isoMassesService.isoMassesForVariant(id);
	}
	
	@GetMapping("/variant/{id}/chart")
	private JsonNode getIsoChartByVariant(@PathVariable int id) {
		return isoMassesService.isoChartForVariant(id);
	}

	@GetMapping("/component/{id}")
	private JsonNode getIsoMassesByCompoent(@PathVariable int id) {
		return isoMassesService.isoMassesForComponent(id);
	}
	
	@GetMapping("/component/{id}/chart")
	private JsonNode getIsoChartByCompoent(@PathVariable int id) {
		return isoMassesService.isoChartForComponent(id);
	}
}
