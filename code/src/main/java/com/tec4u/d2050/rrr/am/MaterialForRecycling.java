package com.tec4u.d2050.rrr.am;

public class MaterialForRecycling {
	private String materialName, materialClass;
	private double recycledContent;
	private MaterialComponent component;

	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}

	public String getMaterialClass() {
		return materialClass;
	}

	public void setMaterialClass(String materialClass) {
		this.materialClass = materialClass;
	}

	public double getRecycledContent() {
		return recycledContent;
	}

	public void setRecycledContent(double recycledContent) {
		this.recycledContent = recycledContent;
	}

	public MaterialComponent getComponent() {
		return component;
	}

	public void setComponent(MaterialComponent component) {
		this.component = component;
	}
	
}
