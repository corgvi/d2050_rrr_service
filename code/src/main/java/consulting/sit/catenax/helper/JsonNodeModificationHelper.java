package consulting.sit.catenax.helper;

import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class JsonNodeModificationHelper {

	/**
	 * Removes elements on level 1 from JsonNode that are not in the collection.
	 *
	 * @param objectNode          ObjectNode to remove elements from
	 * @param elementsNotToRemove Collection with names of elements not to be
	 *                            removed
	 */
	public void removeAllBut(ObjectNode objectNode, Collection<String> elementsNotToRemove) {
		List<String> removeList = new ArrayList<>();
		objectNode.fieldNames().forEachRemaining(e -> {
			if (!elementsNotToRemove.contains(e)) {
				removeList.add(e);
			}
		});
		objectNode.remove(removeList);
	}
}
