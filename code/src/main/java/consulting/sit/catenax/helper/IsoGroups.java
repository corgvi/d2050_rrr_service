package consulting.sit.catenax.helper;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IsoGroups {
	public static final int METALS = 1;
	public static final int POLYMERS = 2;
	public static final int ELASTOMERS = 3;
	public static final int GLASS = 4;
	public static final int FLUIDS = 5;
	public static final int MON = 6;
	public static final int OTHERS = 7;

	private static final HashMap<String, Integer> groups = new HashMap<>(62);
	static {
		groups.put("1", METALS);
		groups.put("1.1", METALS);
		groups.put("1.1.1", METALS);
		groups.put("1.1.2", METALS);
		groups.put("1.2", METALS);
		groups.put("1.2.1", METALS);
		groups.put("1.2.2", METALS);
		groups.put("1.2.3", METALS);
		groups.put("2", METALS);
		groups.put("2.1", METALS);
		groups.put("2.1.1", METALS);
		groups.put("2.1.2", METALS);
		groups.put("2.2", METALS);
		groups.put("2.2.1", METALS);
		groups.put("2.2.2", METALS);
		groups.put("2.3", METALS);
		groups.put("3", METALS);
		groups.put("3.1", METALS);
		groups.put("3.2", METALS);
		groups.put("3.3", METALS);
		groups.put("3.4", METALS);
		groups.put("3.5", METALS);
		groups.put("4", METALS);
		groups.put("4.1", METALS);
		groups.put("4.2", METALS);
		groups.put("5", POLYMERS);
		groups.put("5.1", POLYMERS);
		groups.put("5.1a", POLYMERS);
		groups.put("5.1b", POLYMERS);
		groups.put("5.1 a", POLYMERS);
		groups.put("5.1 b", POLYMERS);
		groups.put("5.1.a", POLYMERS);
		groups.put("5.1.b", POLYMERS);
		groups.put("5.2", ELASTOMERS);
		groups.put("5.3", ELASTOMERS);
		groups.put("5.4", POLYMERS);
		groups.put("5.4.1", POLYMERS);
		groups.put("5.4.2", POLYMERS);
		groups.put("5.4.3", POLYMERS);
		groups.put("5.5", POLYMERS);
		groups.put("5.5.1", POLYMERS);
		groups.put("5.5.2", POLYMERS);
		groups.put("6", POLYMERS);
		groups.put("6.1", POLYMERS);
		groups.put("6.2", POLYMERS);
		groups.put("6.3", ELASTOMERS);
		groups.put("7", OTHERS);
		groups.put("7.1", MON);
		groups.put("7.2", GLASS);
		groups.put("7.3", OTHERS);
		groups.put("8", OTHERS);
		groups.put("8.1", OTHERS);
		groups.put("8.2", OTHERS);
		groups.put("9", FLUIDS);
		groups.put("9.1", FLUIDS);
		groups.put("9.2", FLUIDS);
		groups.put("9.3", FLUIDS);
		groups.put("9.4", FLUIDS);
		groups.put("9.5", FLUIDS);
		groups.put("9.6", FLUIDS);
		groups.put("9.7", OTHERS);
		groups.put("9.8", OTHERS);
	}
	
	public static List<Integer> groups(){
		return Arrays.asList(METALS,POLYMERS, ELASTOMERS, GLASS, FLUIDS, MON, OTHERS);
	}

	public static int fromVda(String vdaUg) {
		log.info("vdaUg: ", vdaUg);
		return groups.getOrDefault(vdaUg, 0);
	}
}
